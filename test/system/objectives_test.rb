require "application_system_test_case"

class ObjectivesTest < ApplicationSystemTestCase
  setup do
    @objective = objectives(:one)
  end

  test "visiting the index" do
    visit objectives_url
    assert_selector "h1", text: "Objectives"
  end

  test "should create objective" do
    visit objectives_url
    click_on "New objective"

    fill_in "End", with: @objective.end
    fill_in "Goal", with: @objective.goal
    fill_in "Metric", with: @objective.metric
    fill_in "Result", with: @objective.result
    click_on "Create Objective"

    assert_text "Objective was successfully created"
    click_on "Back"
  end

  test "should update Objective" do
    visit objective_url(@objective)
    click_on "Edit this objective", match: :first

    fill_in "End", with: @objective.end
    fill_in "Goal", with: @objective.goal
    fill_in "Metric", with: @objective.metric
    fill_in "Result", with: @objective.result
    click_on "Update Objective"

    assert_text "Objective was successfully updated"
    click_on "Back"
  end

  test "should destroy Objective" do
    visit objective_url(@objective)
    click_on "Destroy this objective", match: :first

    assert_text "Objective was successfully destroyed"
  end
end
