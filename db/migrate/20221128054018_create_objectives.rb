class CreateObjectives < ActiveRecord::Migration[7.0]
  def change
    create_table :objectives do |t|
      t.integer :company_id
      t.date :end
      t.string :metric
      t.integer :goal
      t.integer :result

      t.timestamps
    end
  end
end
