class RemoveMetricFromCompany < ActiveRecord::Migration[7.0]
  def change
    remove_column :companies, :metric
  end
end
