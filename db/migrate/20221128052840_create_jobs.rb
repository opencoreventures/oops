class CreateJobs < ActiveRecord::Migration[7.0]
  def change
    create_table :jobs do |t|
      t.integer :user_id
      t.integer :company_id
      t.date :start
      t.date :end
      t.string :title

      t.timestamps
    end
  end
end
