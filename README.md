# README

This is the codebase for [Open core ventures](https://opencoreventures.com/) OPerationS (OOPS).

This is a Rails 7 codebase written in Ruby 3.

Copyright Open Core Ventures.

TODOs are on [GitLab](https://gitlab.com/opencoreventures/oops/-/issues)

Use 'fly deploy' to deploy to Fly.io at [https://oops.fly.dev/](https://oops.fly.dev/).

gem pg failing? try:

  brew install libpq
  gem install pg -- --with-pg-include=/opt/homebrew/opt/libpq/include --with-pg-lib=/opt/homebrew/opt/libpq/lib

Use fixture data in dev? rails db:fixtures:load
