Rails.application.routes.draw do
  resources :objectives
  resources :investments
  resources :jobs
  resources :companies
  get 'pages/home'
  get 'sessions/new'
  resources :users
  get 'signup', to: 'users#new', as: 'signup'
  get 'login', to: 'sessions#new', as: 'login'
  get 'logout', to: 'sessions#destroy', as: 'logout'
  resources :users
  resources :sessions

  # Defines the root path route ("/")
  root "pages#home"
end
