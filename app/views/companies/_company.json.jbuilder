json.extract! company, :id, :name, :metric, :created_at, :updated_at
json.url company_url(company, format: :json)
