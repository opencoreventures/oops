json.extract! job, :id, :user_id, :company_id, :start, :end, :title, :created_at, :updated_at
json.url job_url(job, format: :json)
