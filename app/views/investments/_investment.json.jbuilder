json.extract! investment, :id, :user_id, :company_id, :amount, :status, :created_at, :updated_at
json.url investment_url(investment, format: :json)
