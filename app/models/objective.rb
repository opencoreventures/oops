class Objective < ApplicationRecord
  has_one :company

  def self.metric_options
    %w(views contributors installations arr)
  end

  validates :metric, inclusion: { in: self.metric_options }
end
