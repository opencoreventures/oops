class Investment < ApplicationRecord
  has_one :user
  has_one :company

  def self.status_options
    %w(interest invited deck associate partner partners dd terms docs wired)
  end

  validates :status, inclusion: { in: self.status_options}
end
