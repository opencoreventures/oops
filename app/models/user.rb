class User < ApplicationRecord
  has_secure_password
  has_many :jobs
  has_many :investments
  
  # A user be an OCV/company employee/contractor (see jobs table), and/or investor (see investments table)

  validates_uniqueness_of :email
end
