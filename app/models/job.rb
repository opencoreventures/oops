class Job < ApplicationRecord
  has_one :user
  has_one :company
end
