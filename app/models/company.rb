class Company < ApplicationRecord
  has_many :jobs
  has_many :investments
  has_many :objectives
end
